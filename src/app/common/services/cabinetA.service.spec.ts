import { TestBed, inject } from '@angular/core/testing';

import { cabinetAService } from './cabinetA.service';

describe('cabinetAService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [cabinetAService]
    });
  });

  it('should be created', inject([cabinetAService], (service: cabinetAService) => {
    expect(service).toBeTruthy();
  }));
});
