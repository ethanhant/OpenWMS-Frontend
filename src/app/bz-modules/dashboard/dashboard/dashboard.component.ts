import { Component, OnInit , ViewChild , ElementRef} from '@angular/core';
import Color from '../../../utils/color.util';
import * as echarts from 'echarts';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit  {
  @ViewChild('line') line: ElementRef;
  @ViewChild('bar') bar: ElementRef;
  lineEchart;
  barEchart;
  public barChart = {
    title: {
      text: '机柜功率监测',
      subtext: '测试',
      x: 'center'
    },
    color: Color.baseColor,
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        // 坐标轴指示器，坐标轴触发有效
        type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
      },
      formatter: '{b}月{a}:{c}'
    },
    grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true
    },
    xAxis: [
      {
        type: 'category',
        data: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
        axisTick: {
          alignWithLabel: true
        }
      }
    ],
    yAxis: [
      {
        type: 'value',
        axisLabel: {
          formatter: '{value} KW'
        }
      }
    ],
    series: [
      {
        name: '实时功率',
        type: 'bar',
        barWidth: '60%',
        itemStyle: {
          normal: {
            color: params => {
              const color = Color.genColor(this.barChart.series[0].data);
              return color[params.dataIndex];
            }
          }
        },
        data: [3.5, 4, 4.2, 3, 2.8, 4.4, 3, 4, 4, 4.2, 3.9, 4.1]
      }
    ]
  };

  public lineChart = {
    title: {
      text: '机柜电流监测',
      subtext: '测试',
      x: 'center'
    },
    color: Color.baseColor,
    tooltip: {
      trigger: 'axis'
    },
    xAxis: {
      type: 'category',
      boundaryGap: false,
      data: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']
    },
    yAxis: {
      type: 'value',
      axisLabel: {
        formatter: '{value} A'
      }
    },
    series: [
      {
        name: 'A路实时电流',
        type: 'line',
        data: [11, 11, 15, 13, 12, 13, 10, 12, 10, 13, 16, 19]
      },
      {
        name: 'B路实时电流',
        type: 'line',
        data: [11, 11, 15, 13, 12, 13, 20, 13, 20, 19, 16, 19]
      }
    ]
  };

  constructor() {}

  ngOnInit() {
   this.lineEchart = echarts.init(this.line.nativeElement);
   this.lineEchart.setOption(this.lineChart)
   this.barEchart = echarts.init(this.bar.nativeElement);
   this.barEchart.setOption(this.barChart)
  }
}
