import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { cabinetAComponent } from './cabinetA.component';

describe('cabinetAComponent', () => {
  let component: cabinetAComponent;
  let fixture: ComponentFixture<cabinetAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ cabinetAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(cabinetAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
