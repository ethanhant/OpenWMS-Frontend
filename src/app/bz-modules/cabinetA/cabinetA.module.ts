import { NgModule } from '@angular/core';
import {SharedModule as shareModule} from '../../common/shared.module'
import { RouterModule } from '@angular/router';

import { DataTableModule } from 'primeng/components/datatable/datatable';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import { TabViewModule } from 'primeng/components/tabview/tabview';

import { SharedModule } from '../../common/shared.module';
import { cabinetAComponent } from './cabinetA.component';
import { cabinetATableComponent } from './cabinetA-table/cabinetA-table.component';
import { cabinetAItemDetailComponent } from './cabinetA-item-detail/cabinetA-item-detail.component';
import { InboundReceiptTableComponent } from './inbound-receipt-table/inbound-receipt-table.component';
import { InboundReceiptDetailComponent } from './inbound-receipt-detail/inbound-receipt-detail.component';
import { OutboundReceiptTableComponent } from './outbound-receipt-table/outbound-receipt-table.component';
import { OutboundReceiptDetailComponent } from './outbound-receipt-detail/outbound-receipt-detail.component';
import { NewInboundReceiptComponent } from './new-inbound-receipt/new-inbound-receipt.component';
import { cabinetAService } from '../../common/services/cabinetA.service';
import { NewOutboundReceiptComponent } from './new-outbound-receipt/new-outbound-receipt.component';

import { cabinetARoutes } from './cabinetA.routes';

@NgModule({
  imports: [
    shareModule,
    SharedModule,
    DataTableModule,
    CalendarModule,
    DropdownModule,
    TabViewModule,
    RouterModule.forChild(cabinetARoutes)
  ],
  declarations: [
    cabinetAComponent,
    cabinetATableComponent,
    cabinetAItemDetailComponent,
    InboundReceiptTableComponent,
    InboundReceiptDetailComponent,
    OutboundReceiptTableComponent,
    OutboundReceiptDetailComponent,
    NewInboundReceiptComponent,
    NewOutboundReceiptComponent
  ],
  providers:[
    cabinetAService
  ]
})
export class cabinetAModule {

}
