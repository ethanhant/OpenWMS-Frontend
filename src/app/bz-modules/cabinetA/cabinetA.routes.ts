import { cabinetAComponent } from './cabinetA.component';
import { cabinetATableComponent } from './cabinetA-table/cabinetA-table.component';
import { cabinetAItemDetailComponent } from './cabinetA-item-detail/cabinetA-item-detail.component';
import { InboundReceiptTableComponent } from './inbound-receipt-table/inbound-receipt-table.component';
import { InboundReceiptDetailComponent } from './inbound-receipt-detail/inbound-receipt-detail.component';
import { OutboundReceiptTableComponent } from './outbound-receipt-table/outbound-receipt-table.component';
import { OutboundReceiptDetailComponent } from './outbound-receipt-detail/outbound-receipt-detail.component';
import { NewInboundReceiptComponent } from './new-inbound-receipt/new-inbound-receipt.component';
import { NewOutboundReceiptComponent } from './new-outbound-receipt/new-outbound-receipt.component';

export const cabinetARoutes = [{
	path: '',
	component: cabinetAComponent,
	children: [
		{ path: '', redirectTo: 'cabinetAtable/page/1', pathMatch: 'full' },
		{ path: 'cabinetA-table/page/:page', component: cabinetATableComponent },
		{ path: 'cabinetA-item-detail/item-id/:item-id', component: cabinetAItemDetailComponent },
		{ path: 'inbound-receipt-table/page/:page', component: InboundReceiptTableComponent },
		{ path: 'inbound-receipt-detail/receipt-no/:receipt-no', component: InboundReceiptDetailComponent },
		{ path: 'new-inbound-receipt/receipt-no/:receipt-no', component: NewInboundReceiptComponent },
		{ path: 'outbound-receipt-table/page/:page', component: OutboundReceiptTableComponent },
		{ path: 'outbound-receipt-detail/receipt-no/:receipt-no', component: OutboundReceiptDetailComponent },
		{ path: 'new-outbound-receipt/receipt-no/:receipt-no', component: NewOutboundReceiptComponent }
	]
}];
