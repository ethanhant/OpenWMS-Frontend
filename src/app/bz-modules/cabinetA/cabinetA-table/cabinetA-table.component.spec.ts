import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { cabinetATableComponent } from './cabinetA-table.component';

describe('cabinetATableComponent', () => {
  let component: cabinetATableComponent;
  let fixture: ComponentFixture<cabinetATableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ cabinetATableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(cabinetATableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
