import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { cabinetAItemDetailComponent } from './cabinetA-item-detail.component';

describe('cabinetAItemDetailComponent', () => {
  let component: cabinetAItemDetailComponent;
  let fixture: ComponentFixture<cabinetAItemDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [cabinetAItemDetailComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(cabinetAItemDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
